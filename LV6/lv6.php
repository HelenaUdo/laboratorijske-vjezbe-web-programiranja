<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>LV6 php</title>
    <h1>LV6 - prvi zadatak</h1>
</head>

<body>
    <?php
    $student = array("Helena","Udovicic","125346342244","A2340","3. godina");

    echo "<br> STUDENT <br>";
    foreach($student as $value) {
        echo "$value <br>";
    }

    $automobili = array(
        "Citroen" => array(
            "tip_automobila" => array("C3", "C6", "C9"),
            "kubikaza" => array( 150, 200, 250),
            "boja" => array("crna", "plava", "crvena"),
            "godina_proizvodnje" => array(2000, 2010, 2020),
            "motor" => array("dizel", "benzin", "dizel")
        ),
        "Mercedes" => array(
            "tip_automobila" => array("A3", "A5", "A6"),
            "kubikaza" => array( 200, 250, 300),
            "boja" => array("siva", "plava", "crna"),
            "godina_proizvodnje" => array(2000, 2010, 2020),
            "motor" => array("benzin", "dizel", "benzin")
        )
    );

    echo "<br> AUTOMOBILI <br>";
    echo "<br> Citroeni: <br>";
    for($i = 0; $i < 3; $i++) {
        foreach ($automobili["Citroen"] as $citroen => $value) {
            echo "$citroen : $value[$i]</br>";
        }
        echo "</br>";
    }


    echo "</br>Mercedesi: </br>";
    for($i = 0; $i < 3; $i++){
        echo "Tip Automobila: ".$automobili["Mercedes"]["tip_automobila"][$i]."</br>";
        echo "Kubikaža: ".$automobili["Mercedes"]["kubikaza"][$i]."</br>";
        echo "Boja: ".$automobili["Mercedes"]["boja"][$i]."</br>";
        echo "Godina prozvodnje: ".$automobili["Mercedes"]["godina_proizvodnje"][$i]."</br>";
        echo "Motor: ".$automobili["Mercedes"]["motor"][$i]."</br></br>";
    }

    function kvadrat($broj){
        $suma = $broj * $broj;
        echo "Kvadrat broja $broj je: $suma <br> <br>";
    } 

    kvadrat("3");

    class Kupac{
            public $ime;
            public $prezime;

            public function __construct($ime, $prezime) {
                $this->ime = $ime;
                $this->prezime = $prezime;
            }

            function postaviIme($ime){
                $this->ime = $ime;
            }

            function postaviPrezime($prezime){
                $this->prezime = $prezime;
            }

            function ispis(){
                echo 'Kupac je: ' . $this->ime . ' ' . $this->prezime;
            }
    }

    $kupac = new Kupac("Helena", "Udovicic");
    $kupac->ispis();

    ?>
</body>
</html>